<?php

if (!defined('_PS_VERSION_')) {
  exit;
}

class FacebookMessenger extends Module
{

  public $fb_url;
  public $fb_enabled;
  public $fb_lang;
  public $fb_appId;

  public function __construct()
  {
    $this->name = 'facebookmessenger';
    $this->tab = 'front_office_features';
    $this->version = '1.0';
    $this->author = 'Mateusz Prasał';

    $this->bootstrap = true;

    parent::__construct();

    $this->displayName = $this->l('Facebook Messenger');
    $this->description = $this->l('Wysyłka wiadomości bezpośrednio na Twój fanpage Facebook');
    $this->confirmUninstall = $this->l('Czy na pewno chcesz usunąć moduł Facebook Messenger?');

    if (!Configuration::get('FACEBOOKMESSENGER_MODULE')) {
      $this->warning = $this->l('Brak modułu');
    }

    $this->fb_url = strval(Configuration::get('FBMESSENGERBOX_URL'));
    $this->fb_enabled = Configuration::get('FBMESSENGERBOX_ENABLED');
    $this->fb_lang = Configuration::get('FBMESSENGERBOX_LANG');
    $this->fb_appId = Configuration::get('FBMESSENGERBOX_APPID');
  }

  public function install()
  {
    if (!parent::install() || !$this->registerHook('displayFooter') || !$this->registerHook('displayHeader') ) {
      return false;

    }
    return true;
  }

  public function uninstall()
  {
    if (!parent::uninstall() ||
      !Configuration::deleteByName('FACEBOOKMESSENGER_MODULE')
    )
      return false;

    return true;
  }

  public function getContent()
  {

    $output = '';
    if (Tools::isSubmit('submit'.$this->name)) {
      $url = strval(Tools::getValue('FBMESSENGERBOX_URL'));
      $lang = Tools::getValue('FBMESSENGERBOX_LANG');
      $appid = Tools::getValue('FBMESSENGERBOX_APPID');
      // $color = Tools::getValue('FBMESSENGERBOX_COLOR');
      $enabled = Tools::getValue('FBMESSENGERBOX_ENABLED');

      if (!$enabled || !intval($enabled)) {

        Configuration::updateValue('FBMESSENGERBOX_ENABLED',0);
      } else {
        Configuration::updateValue('FBMESSENGERBOX_ENABLED',1);
      }
      if (!$url || empty($url)) {
        $output .= $this->displayError('Wartość URL nie może być pusta!');
        Configuration::updateValue('FBMESSENGERBOX_ENABLED',0);
      } else {
        Configuration::updateValue('FBMESSENGERBOX_URL',$url);
        Configuration::updateValue('FBMESSENGERBOX_APPID',$appid);
        $output .= $this->displayConfirmation('W porządku!');

      }

      if (!$lang || empty($lang)) {
        $output .= $this->displayError('Wartość LANG nie może być pusta!');
      } else {
        Configuration::updateValue('FBMESSENGERBOX_LANG',$lang);
      }
    }

    $this->context->smarty->assign(array(
      'form' => $this->displayForm(),
      'output' => $output,
      'module_dir' => _MODULE_DIR_
    ));
    return $this->display(__FILE__,'config.tpl');
  }

  public function displayForm()
  {
    // Get default language
    $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
    $languages = [
      'pl_PL' => 'Polski',
      'en_US' => 'Angielski (Stany Zjednoczone)'
    ];
    // Init Fields form array
    $fields_form[0]['form'] = array(
        'legend' => array(
            'title' => $this->l('Ustawienia'),
        ),

        'input' => array(
          array(
             'type' => 'switch',
             'label' => $this->l('Włącz Facebook Messenger Box'),
             'name' => 'FBMESSENGERBOX_ENABLED',
             'is_bool' => true,
             'values' => array(
                 array(
                   'id' => 'active_on',
                   'value' => 1,
                   'label' => $this->l('Tak')
                 ),
                 array(
                   'id' => 'active_off',
                   'value' => 0,
                   'label' => $this->l('Nie')
                 )
             ),
           ),
           array(
               'type' => 'text',
               'label' => $this->l('App ID:'),

               'name' => 'FBMESSENGERBOX_APPID',
               'size' => 20,
               'required' => true
           ),
          array(
              'type' => 'text',
              'label' => $this->l('Page ID Twojej strony:'),
              'hint' => 'Użyj https://findmyfbid.com/ aby dowiedzieć się o swoim numerze ID',
              'name' => 'FBMESSENGERBOX_URL',
              'size' => 20,
              'required' => true
          ),
          	array(
          	  'type' => 'select',
          	  'label' => $this->l('Język Messenger Box'),
          	  'name' => 'FBMESSENGERBOX_LANG',
          	  'required' => true,
          	  'options' => array(
                'query' => array(
                    array('key' => 'pl_PL', 'name' => 'Polski'),
                    array('key' => 'en_US', 'name' => 'Angielski'),
                ),
                'id' => 'key',
                'name' => 'name'
          	  )
          	),

        ),
        'submit' => array(
            'title' => $this->l('Save'),
            'class' => 'btn btn-default pull-right'
        )
    );


    $helper = new HelperForm();

    // Module, token and currentIndex
    $helper->module = $this;
    $helper->name_controller = $this->name;
    $helper->token = Tools::getAdminTokenLite('AdminModules');
    $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

    // Language
    $helper->default_form_language = $default_lang;
    $helper->allow_employee_form_lang = $default_lang;

    // Title and toolbar
    $helper->title = $this->displayName;
    $helper->show_toolbar = true;        // false -> remove toolbar
    $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
    $helper->submit_action = 'submit'.$this->name;
    $helper->toolbar_btn = array(
        'save' =>
        array(
            'desc' => $this->l('Save'),
            'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
            '&token='.Tools::getAdminTokenLite('AdminModules'),
        ),
        'back' => array(
            'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
            'desc' => $this->l('Back to list')
        )
    );

    // Load current value
    $helper->fields_value['FBMESSENGERBOX_URL'] = Configuration::get('FBMESSENGERBOX_URL');
    $helper->fields_value['FBMESSENGERBOX_LANG'] = Configuration::get('FBMESSENGERBOX_LANG');
    $helper->fields_value['FBMESSENGERBOX_APPID'] = Configuration::get('FBMESSENGERBOX_APPID');
    $helper->fields_value['FBMESSENGERBOX_ENABLED'] = Configuration::get('FBMESSENGERBOX_ENABLED');


    return $helper->generateForm($fields_form);
  }



  public function hookdisplayFooter($params)
  {

    $this->context->smarty->assign(array(
      'fb_url'=> $this->fb_url,
      'fb_lang'=>$this->fb_lang,
      'fb_enabled' => $this->fb_enabled,
      'this_module' => $this->_path,
      'appId' => $this->fb_appId


    ));
    return $this->display(__FILE__,'facebookmessenger.tpl');
  }


}



{if $fb_enabled == true}


{literal}
<script>

  window.fbAsyncInit = function() {
    FB.init({
      appId      : '{$appId}',
      xfbml      : true,
      version    : 'v2.6'
    });
  };

  (function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/{$fb_lang}/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

</script>
{/literal}



<div class="fb-customerchat" page_id="{$fb_url}" minimized="true">

</div>





{/if}
